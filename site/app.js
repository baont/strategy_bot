document.addEventListener("DOMContentLoaded", function() {
    const urlParams = new URLSearchParams(window.location.search);
    const alertId = urlParams.get('alert_id');
    if (alertId) {
        const userId = urlParams.get('user_id');
        const data = urlParams.get('alert_data');
        fetchExistingAlertData(userId, alertId, data);
    } else {
        populateTokens();
        updateParameterInputs();
        updateIndicatorConditions();
    }
});

function populateTokens() {
    const tokens = ["BTC", "ETH", "LTC", "XRP"]; // This should ideally come from an API
    const tokenSelect = document.getElementById("token-select");
    tokens.forEach(token => {
        let option = document.createElement("option");
        option.value = token;
        option.textContent = token;
        tokenSelect.appendChild(option);
    });
}

function addInput(container, label, id, defaultValue) {
    let div = document.createElement('div');
    let labelElement = document.createElement('label');
    labelElement.textContent = label;
    let input = document.createElement('input');
    input.type = 'number';
    input.id = id;
    input.value = defaultValue

    div.appendChild(labelElement);
    div.appendChild(input);
    container.appendChild(div);
}

function updateParameterInputs() {
    const indicator = document.getElementById("indicator-select").value;
    const container = document.getElementById("parameters-container");
    container.innerHTML = ''; // Clear previous inputs

    switch (indicator) {
        case 'macd':
            addInput(container, 'Short-term EMA', 'short_ema', 12);
            addInput(container, 'Long-term EMA', 'long_ema', 26);
            addInput(container, 'Signal line EMA', 'signal_ema', 9);
            break;
        case 'rsi':
            addInput(container, 'Period', 'rsi_period', 14);
            break;
        case 'moving-average':
            addInput(container, 'Period', 'ma_period', 20);
            break;
    }
}

function fetchExistingAlertData(userId, alertId, json_data) {
    const data = JSON.parse(json_data);
    console.log(userId)
    console.log(alertId)
    console.log("init data:", JSON.stringify(data, null, 2));    
    populateFormWithAlertData(data);
}

function updateIndicatorConditions() {
    const indicator = document.getElementById("indicator-select").value;
    const container = document.getElementById("condition-container");
    container.innerHTML = ''; // Clear existing conditions

    let htmlContent = '';
    switch(indicator) {
        case 'macd':
            htmlContent += createRadioOption("macd_cross_above", "MACD Line Crosses Above the Signal Line", true);
            htmlContent += createRadioOption("macd_cross_below", "MACD Line Crosses Below the Signal Line");
            htmlContent += createRadioOption("zero_cross_up", "Zero Line Cross Up");
            htmlContent += createRadioOption("zero_cross_down", "Zero Line Cross Down");
            break;
        case 'rsi':
            htmlContent = `
                <div >
                    <input type="radio" id="rsi_overbought_check" name="indicator_condition" value="price_above_ma" style="display:inline-block;" checked>
                    <label for="rsi_overbought_threshold" style="display:inline-block;">RSI Overbought Threshold ></label>
                    <input type="number" id="rsi_overbought_threshold" name="rsi_overbought_threshold" value="70" style="display:inline-block;">
                </div>
                <div>
                    <input type="radio" id="rsi_oversold_check" name="indicator_condition" value="price_above_ma" style="display:inline-block;">
                    <label for="rsi_oversold_threshold" style="display:inline-block;">RSI Oversold Threshold <</label>
                    <input type="number" id="rsi_oversold_threshold" name="rsi_oversold_threshold" value="30" style="display:inline-block;">
                </div>`;

            break;
        case 'moving-average':``
            htmlContent = `
                <input type="radio" id="price_above_ma" name="indicator_condition" value="price_above_ma" checked>
                <label for="price_above_ma"  style="display:inline-block;">Price Crosses Above Moving Average</label><br>
                <input type="radio" id="price_below_ma" name="indicator_condition" value="price_below_ma">
                <label for="price_below_ma"  style="display:inline-block;">Price Crosses Below Moving Average</label>`;
            break;
    }

    container.innerHTML = htmlContent;
}

function createRadioOption(id, label, isChecked = false) {
    return `<div>
                <input type="radio" id="${id}" name="indicator_condition" value="${id}" ${isChecked ? 'checked' : ''}>
                <label for="${id}" class="inline-label">${label}</label>
            </div>`;
}

function populateFormWithAlertData(alert) {
    console.log(`data received ${JSON.stringify(alert, null, 2)}`)

    populateTokens();
    document.getElementById("token-select").value = alert.token;
    document.getElementById("timestamp-select").value = alert.timestamp;
    document.getElementById("indicator-select").value = alert.indicator;

    updateParameterInputs();
    updateIndicatorConditions();

    if (alert.parameters.shortEMA) {
        document.getElementById('short_ema').value = alert.parameters.shortEMA;
        document.getElementById('long_ema').value = alert.parameters.longEMA;
        document.getElementById('signal_ema').value = alert.parameters.signalEMA;
    }
    if (alert.parameters.rsiPeriod) {
        document.getElementById('rsi_period').value = alert.parameters.rsiPeriod;
    }
    if (alert.parameters.maPeriod) {
        document.getElementById('ma_period').value = alert.parameters.maPeriod;
    }

    if (alert.conditions.macdCrossAbove) {
        document.getElementById('macd_cross_above').checked = true;
    }
    if (alert.conditions.macdCrossBelow) {
        document.getElementById('macd_cross_below').checked = true;
    }
    if (alert.conditions.zeroCrossUp) {
        document.getElementById('zero_cross_up').checked = true;
    }
    if (alert.conditions.zeroCrossDown) {
        document.getElementById('zero_cross_down').checked = true;
    }

    if (alert.conditions.rsiOverboughtThreshold) {
        document.getElementById('rsi_overbought_threshold').value = alert.conditions.rsiOverboughtThreshold;
        document.getElementById('rsi_overbought_check').checked = true;
    }
    if (alert.conditions.rsiOversoldThreshold) {
        document.getElementById('rsi_oversold_threshold').value = alert.conditions.rsiOversoldThreshold;
        document.getElementById('rsi_oversold_check').checked = true;
    }

    if (alert.conditions.priceCrossesAboveMA) {
        document.getElementById('price_above_ma').checked = true;
    }
    if (alert.conditions.priceCrossesBelowMA) {
        document.getElementById('price_below_ma').checked = true;
    }
}

function submit() {
    const token = document.getElementById("token-select").value;
    const timestamp = document.getElementById("timestamp-select").value;
    const indicator = document.getElementById("indicator-select").value;
    let conditions = {};

    // Check which indicator is selected and gather additional data
    switch (indicator) {
        case 'macd':
            // Assuming you would have inputs for MACD conditions if needed
            conditions.macdCrossAbove = document.querySelector('input[name="indicator_condition"][value="macd_cross_above"]').checked;
            conditions.macdCrossBelow = document.querySelector('input[name="indicator_condition"][value="macd_cross_below"]').checked;
            conditions.zeroCrossUp = document.querySelector('input[name="indicator_condition"][value="zero_cross_up"]').checked;
            conditions.zeroCrossDown = document.querySelector('input[name="indicator_condition"][value="zero_cross_down"]').checked;
            break;
        case 'rsi':
            // Collect RSI overbought and oversold thresholds if the corresponding radio is selected
            if (document.getElementById('rsi_overbought_check').checked) {
                conditions.rsiOverboughtThreshold = document.getElementById("rsi_overbought_threshold").value;
            }
            if (document.getElementById('rsi_oversold_check').checked) {
                conditions.rsiOversoldThreshold = document.getElementById("rsi_oversold_threshold").value;
            }
            break;
        case 'moving-average':
            // Collect conditions for moving average crossings
            conditions.priceCrossesAboveMA = document.getElementById('price_above_ma').checked;
            conditions.priceCrossesBelowMA = document.getElementById('price_below_ma').checked;
            break;
    }

    // Define additional parameters based on the indicator selected, these IDs must match those created dynamically
    const additionalParams = {};
    if (document.getElementById('short_ema')) {
        additionalParams.shortEMA = document.getElementById('short_ema').value;
        additionalParams.longEMA = document.getElementById('long_ema').value;
        additionalParams.signalEMA = document.getElementById('signal_ema').value;
    }
    if (document.getElementById('rsi_period')) {
        additionalParams.rsiPeriod = document.getElementById('rsi_period').value;
    }
    if (document.getElementById('ma_period')) {
        additionalParams.maPeriod = document.getElementById('ma_period').value;
    }

    const urlParams = new URLSearchParams(window.location.search);
    const isEditParam = urlParams.get('is_edit');
    
    const isEdit = isEditParam && isEditParam == 'true';
    let alertId = undefined;
    let userId = undefined;
    if (isEdit) {
        alertId = urlParams.get('alert_id');
        userId = urlParams.get('user_id');
    }

    const data = {
        is_new: !isEdit,
        alert_id: alertId,
        user_id: userId,
        alert: {
            token: token,
            timestamp: timestamp,
            indicator: indicator,
            conditions: conditions,
            parameters: additionalParams
        }
    };

    console.log("Data collected:", JSON.stringify(data, null, 2));
    window.Telegram.WebApp.sendData(JSON.stringify(data));
    window.Telegram.WebApp.close()

    // fetch('http://127.0.0.1:8000/alert-data', {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify(data)
    // })
    //   .then(response => response.json())
    //   .then(result => {
    //       console.log('Success:', result);
    //       window.Telegram.WebApp.close()
    //   })
    //   .catch(error => {
    //       console.error('Error:', error);
    //       window.Telegram.WebApp.close()
    //   });
}